from config.database import Base
from sqlalchemy import Column, Integer, String, Float

#Crear una clase de tipo Base
class Movie(Base):
    #Creacion de tabla
    __tablename__ = "movies"  #tablename: para el nombre de la tabla que estamos haciendo

    id = Column(Integer, primary_key = True)
    tittle = Column(String)
    overview = Column(String)
    year = Column(Integer)
    rating = Column(Float)
    category = Column(String)